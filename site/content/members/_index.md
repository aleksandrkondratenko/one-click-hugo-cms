---
title: "Солисты"
image: /img/about-jumbotron.jpg
values:
  - heading: "Светлана Власова"
    text: >
      Художественный руководитель
   imageUrl: "/img/about-shade-grown.jpg"
  - heading: "Елизавета Дзуцева"
    text: >
      Single-origin coffee is coffee grown within a single known
      geographic origin. Sometimes, this is a single farm or a
      specific collection of beans from a single country. The name of
      the coffee is then usually the place it was grown to whatever
      degree available.
    imageUrl: "/img/about-single-origin.jpg"
  - heading: "Александра Дербенцева"
    text: >
      We want to truly empower the communities that bring amazing
      coffee to you. That’s why we reinvest 20% of our profits into
      farms, local businesses and schools everywhere our coffee is
      grown. You can see the communities grow and learn more about
      coffee farming on our blog.
    imageUrl: "/img/about-direct-sourcing.jpg"
  - heading: "Анастасия Петухова"
    text: >
      We want to truly empower the communities that bring amazing
      coffee to you. That’s why we reinvest 20% of our profits into
      farms, local businesses and schools everywhere our coffee is
      grown. You can see the communities grow and learn more about
      coffee farming on our blog.
    imageUrl: "/img/about-reinvest-profits.jpg"
  - heading: "Екатерина Панкратова"
    text: >
      We want to truly empower the communities that bring amazing
      coffee to you. That’s why we reinvest 20% of our profits into
      farms, local businesses and schools everywhere our coffee is
      grown. You can see the communities grow and learn more about
      coffee farming on our blog.
    imageUrl: "/img/about-reinvest-profits.jpg"
  - heading: "Юлия Рыжова"
    text: >
      We want to truly empower the communities that bring amazing
      coffee to you. That’s why we reinvest 20% of our profits into
      farms, local businesses and schools everywhere our coffee is
      grown. You can see the communities grow and learn more about
      coffee farming on our blog.
    imageUrl: "/img/about-reinvest-profits.jpg"
  - heading: "Екатерина Панкратова"
    text: >
      We want to truly empower the communities that bring amazing
      coffee to you. That’s why we reinvest 20% of our profits into
      farms, local businesses and schools everywhere our coffee is
      grown. You can see the communities grow and learn more about
      coffee farming on our blog.
    imageUrl: "/img/about-reinvest-profits.jpg"
  - heading: "Елена Лапина"
    text: >
      Sustainable agriculture is farming in sustainable ways based on
      an understanding of ecosystem services, the study of
      relationships between organisms and their environment. What
      grows where and how it is grown are a matter of choice and
      careful consideration for nature and communities.
    imageUrl: "/img/about-sustainable-farming.jpg"
  - heading: "Виктория Кондратенко"
    text: >
      Sustainable agriculture is farming in sustainable ways based on
      an understanding of ecosystem services, the study of
      relationships between organisms and their environment. What
      grows where and how it is grown are a matter of choice and
      careful consideration for nature and communities.
    imageUrl: "/img/about-sustainable-farming.jpg"
  - heading: "Семён Рыжов"
    text: >
      Sustainable agriculture is farming in sustainable ways based on
      an understanding of ecosystem services, the study of
      relationships between organisms and their environment. What
      grows where and how it is grown are a matter of choice and
      careful consideration for nature and communities.
    imageUrl: "/img/about-sustainable-farming.jpg"
  - heading: "Денис Михайлов"
    text: >
      Sustainable agriculture is farming in sustainable ways based on
      an understanding of ecosystem services, the study of
      relationships between organisms and their environment. What
      grows where and how it is grown are a matter of choice and
      careful consideration for nature and communities.
    imageUrl: "/img/about-sustainable-farming.jpg"
  - heading: "Александр Кондратенко"
    text: >
      Sustainable agriculture is farming in sustainable ways based on
      an understanding of ecosystem services, the study of
      relationships between organisms and their environment. What
      grows where and how it is grown are a matter of choice and
      careful consideration for nature and communities.
    imageUrl: "/img/about-sustainable-farming.jpg"
---
